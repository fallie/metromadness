package com.unimelb.swen30006.metromadness.tracks;

import java.util.ArrayList;
import java.util.stream.Collectors;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.stations.ActionableStation;
import com.unimelb.swen30006.metromadness.stations.Station;

public class Line {
	
	// The colour of this line
	public Color lineColour;
	public Color trackColour;
	
	// The name of this line
	public String name;
	// The actionableStations on this line
	public ArrayList<ActionableStation> actionableStations;
	// The tracks on this line between actionableStations
	public ArrayList<Track> tracks;
		
	// Create a line
	public Line(Color stationColour, Color lineColour, String name){
		// Set the line colour
		this.lineColour = stationColour;
		this.trackColour = lineColour;
		this.name = name;
		
		// Create the data structures
		this.actionableStations = new ArrayList<ActionableStation>();
		this.tracks = new ArrayList<Track>();
	}
	
	
	public void addActionableStation(ActionableStation s, Boolean two_way){
		// We need to build the track if this is adding to existing actionableStations
		if(this.actionableStations.size() > 0){
			// Get the last actionableStation
			Station last = this.actionableStations.get(this.actionableStations.size()-1).station;
			
			// Generate a new track
			Track t;
			if(two_way){
				t = new DualTrack(last.position, s.station.position, this.trackColour);
			} else {
				t = new Track(last.position, s.station.position, this.trackColour);
			}
			this.tracks.add(t);
		}
		
		// Add the actionableStation
		s.registerLine(this);
		this.actionableStations.add(s);
	}
	
	@Override
	public String toString() {
		return "Line [lineColour=" + lineColour + ", trackColour=" + trackColour + ", name=" + name + "]";
	}


	public boolean endOfLine(ActionableStation s) throws Exception{
		if(this.actionableStations.contains(s)){
			int index = this.actionableStations.indexOf(s);
			return (index==0 || index==this.actionableStations.size()-1);
		} else {
			throw new Exception();
		}
	}

	
	
	public Track nextTrack(ActionableStation currentStation, boolean forward) throws Exception {
		if(this.actionableStations.contains(currentStation)){
			// Determine the track index
			int curIndex = this.actionableStations.lastIndexOf(currentStation);
			// Increment to retrieve
			if(!forward){ curIndex -=1;}
			
			// Check index is within range
			if((curIndex < 0) || (curIndex > this.tracks.size()-1)){
				throw new Exception();
			} else {
				return this.tracks.get(curIndex);
			}
			
		} else {
			throw new Exception();
		}
	}
	
	public ActionableStation nextStation(ActionableStation s, boolean forward) throws Exception{
		if(this.actionableStations.contains(s)){
			int curIndex = this.actionableStations.lastIndexOf(s);
			if(forward){ curIndex+=1;}else{ curIndex -=1;}
			
			// Check index is within range
			if((curIndex < 0) || (curIndex > this.actionableStations.size()-1)){
				throw new Exception();
			} else {
				return this.actionableStations.get(curIndex);
			}
		} else {
			throw new Exception();
		}
	}
	
	public void render(ShapeRenderer renderer){
		// Set the color to our line
		renderer.setColor(trackColour);
	
		// Draw all the track sections
		for(Track t: this.tracks){
			t.render(renderer);
		}	
	}

	public ActionableStation getActionableStation(String name) {
		return actionableStations.stream().filter(x -> x.station.name.equals(name)).collect(Collectors.<ActionableStation>toList()).get(0);
	}
	
}
