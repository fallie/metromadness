package com.unimelb.swen30006.metromadness.stations;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;

import com.unimelb.swen30006.metromadness.passengers.Passenger;
import com.unimelb.swen30006.metromadness.passengers.PassengerGenerator;
import com.unimelb.swen30006.metromadness.routers.PassengerRouter;
import com.unimelb.swen30006.metromadness.trains.Train;

import static com.unimelb.swen30006.metromadness.stations.Station.PLATFORMS;

public class ActiveStation extends ActionableStation {

	public PassengerGenerator g;
	public ArrayList<Passenger> waiting;
	public float maxVolume;
	
	public ActiveStation(Station station) {
		super(station);
		this.waiting = new ArrayList<Passenger>();
		this.g = new PassengerGenerator(this, station.lines, station.maxPax);
		this.maxVolume = station.maxPax;
	}
	
	@Override
	public void enter(Train t) throws Exception {
		if(station.trains.size() >= PLATFORMS){
			throw new Exception();
		} else {
			// Add the train
			station.trains.add(t);
			// Add the waiting passengers
			Iterator<Passenger> pIter = this.waiting.iterator();
			while(pIter.hasNext()){
				Passenger p = pIter.next();
				try {
					t.embark(p);
					pIter.remove();
				} catch (Exception e){
					// Do nothing, already waiting
					break;
				}
			}
			
			//Do not add new passengers if there are too many already
			if (this.waiting.size() > maxVolume){
				return;
			}
			// Add the new passenger
			Passenger[] ps = this.g.generatePassengers();
			for(Passenger p: ps){
				try {
					t.embark(p);
				} catch(Exception e){
					this.waiting.add(p);
				}
			}
		}
	}

}
