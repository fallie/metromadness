package com.unimelb.swen30006.metromadness.stations;

import com.unimelb.swen30006.metromadness.trains.Train;

import static com.unimelb.swen30006.metromadness.stations.Station.*;

public class NoEnterCanLeaveStation extends ActionableStation {

    public NoEnterCanLeaveStation(Station station)
    {
        super(station);
    }

    @Override
    public void enter(Train t) throws Exception {
        if(station.trains.size() >= PLATFORMS){
            throw new Exception();
        } else {
            station.trains.add(t);
        }
    }

}
