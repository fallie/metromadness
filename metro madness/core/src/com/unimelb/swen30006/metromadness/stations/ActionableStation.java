package com.unimelb.swen30006.metromadness.stations;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.unimelb.swen30006.metromadness.passengers.Passenger;
import com.unimelb.swen30006.metromadness.tracks.Line;
import com.unimelb.swen30006.metromadness.trains.Train;

import static com.unimelb.swen30006.metromadness.stations.Station.*;

@SuppressWarnings({"unused", "WeakerAccess"})
public abstract class ActionableStation {

    public Station station;

    public ActionableStation(Station station_) {
        station = station_;
    }

    public void registerLine(Line l){
        station.lines.add(l);
    }


    public abstract void enter(Train t) throws Exception;


    public void depart(Train t) throws Exception {
        if(station.trains.contains(t)){
            station.trains.remove(t);
        } else {
            throw new Exception();
        }
    }

    public boolean canEnter(Line l) throws Exception {
        return station.trains.size() < PLATFORMS;
    }

    // Returns departure time in seconds
    public float getDepartureTime() {
        return DEPARTURE_TIME;
    }

    public boolean shouldLeave(Passenger p) {
        return station.router.shouldLeave(this.station, p);
    }

    @Override
    public String toString() {
        return "Station [position=" + station.position + ", name=" + station.name + ", trains=" + station.trains.size()
                + ", router=" + station.router + "]";
    }

    public Passenger generatePassenger(Station s) {
        return new Passenger(station, s);
    }

}
